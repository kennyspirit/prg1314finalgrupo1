/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

/**
 * Fichero: Constantes.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 11-feb-2014
 */
public class Constantes {

  public final static boolean LOG = true;
  public final static String NOMAPLI = "TIENDA";
  public final static String NOMPROYECTO = "prg1314finalgrupo1";
  public final static String AUTORES = "Paco Aldarias";
  public final static String FECHA = "24/03/2014";
  public final static String BDUSUARIO = "admin";
  public final static String BDPASSWORD = "123";
  public final static String FICHEROCLIENTES = "clientes.csv";
  public final static String FICHEROPRODUCTOS = "productos.csv";
  public final static String FICHEROPEDIDOS = "pedidos.csv";
  public final static String FICHEROLINEAS = "lineas.csv";
  public final static String FICHEROCONFIGURA = "configura.csv";
  public final static String FICHERODB4O = "tienda.db4o";
  public final static int ANIO_MES_DIA = 1;
  public final static int DIA_MES_ANIO = 2;
  public final static String DOCUMENTACION = "prg1314finalgrupo1doc.pdf";
  public final static String JAVADOC = "./dist/javadoc/index.html";
  public final static String CONFIGURAXML = "configura.xml";
}
